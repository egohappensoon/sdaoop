package src.com.sda.oop;

public class Circle {
    private  float radius;
    private  float diameter;
    private  float circumference;
    private  float area;



    public void setRadius(float inputRadius) {
        radius = inputRadius * 2;
        diameter = radius * 2;
        calculateCircumference();
        calculateArea();
    }

    private void calculateCircumference() {
        //2 * Math.PI * radius
        circumference = (float) (2 * Math.PI * radius);
    }

    private void calculateArea() {
        //2 * Math.PI * radius
        area = (float) (Math.PI * Math.pow(radius,2));
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", diameter=" + diameter +
                ", circumference=" + circumference +
                ", area=" + area +
                '}';
    }
}
