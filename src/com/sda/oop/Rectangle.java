package src.com.sda.oop;

public class Rectangle {

    private float length;
    private float width;
    private float area;
    private float perimeter;
    private String fillType;
   // private int length;


    public Rectangle(float length_, float width_) throws Exception {
        if (length_ < 1 || width_ < 1){
            throw new Exception("Rectangle shouldn't have side less than 1 = " + width_);
        }
        if (length_ == width_){
            throw new Exception("Rectangle shouldn't equal sides...");
        }
        length = length_;
        width = width_;
        calculateArea();
        calculatePerimeter();
        //paintRectangle();
        //paintEdges();
        paintEdgez();
    }

    private void calculateArea() {
        //area = L * W
        area = length * width;
    }
    private void calculatePerimeter() {
        //perimeter = 2*L + 2*W
        perimeter = 2 * (length +  width);
    }

    protected void paintRectangle(){
        for(int i = 0; i < width; i++){
            for (int j = 0; j < length ; j++) {
                System.out.print(" * ");
            }
            System.out.println();
        }
    }

    /**protected void paintEdges(){
        for(int i = 0; i < width; i++){
            for (int j = 0; j < length ; j++) {
                System.out.print(" # ");
            }
            System.out.println();
        }
    }
     **/
    protected void paintEdgez() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < length; j++) {
                System.out.print(" # ");
            }
            System.out.println();
        }
        for (int count = 0; count < length - 1; count++) {
            System.out.print("#");
            for (int space = 0; space < length- 2; space++) {
                System.out.print(" ");
            }
            System.out.print("#");
            System.out.println();
        }
        for (int i = 0; i < length; i++) {
            System.out.print("#");
        }

    }


    @Override
    public String toString() {
        return "Rectangle{" +
                "length=" + length +
                ", width=" + width +
                ", area=" + area +
                ", perimeter=" + perimeter +
                ", fillType='" + fillType + '\'' +
                '}';
    }
}
